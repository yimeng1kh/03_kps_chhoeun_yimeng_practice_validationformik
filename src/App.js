import './App.css';
import ValidationFromik from './Components/ValidationFromik';

function App() {
  return (
    <div className="App">
     <ValidationFromik/>
    </div>
  );
}

export default App;
