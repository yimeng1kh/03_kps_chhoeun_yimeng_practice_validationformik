import React, { Component } from 'react'
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

const LoginSchema = Yup.object().shape({
    email: Yup.string()
      .email("Email format is invalid")
      .required("Email Format is invalid"),
    password: Yup.string().matches(
      /^(?=.*[A-Za-z])(?=.*d)(?=.*[@$!%*#?&])[A-Za-zd@$!%*#?&]{8,}$/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
    ),
  });

export default class ValidationFromik extends Component {
  render() {
    return (
        <div className="">
            <div className='bg-red-500'>Hi hello world</div>
        <Formik
          initialValues={{
            email: "",
            password: "",
          }}
          validationSchema={LoginSchema}
          onSubmit={(value) => {
            //same shape as initial values
            console.log(value);
          }}
        >
          {({ errors, touched }) => (
            <Form>
              <Field name="email" type="email" />
              {errors.email && touched.email ? <div>{errors.email}</div> : null}
              <br></br>
              <Field name="password" type="password" />
              {errors.password && touched.password ? (
                <div>{errors.password}</div>
              ) : null}
              <button type="submit">Submit</button>
              <div class="flex justify-center space-x-2">
            </div>
            </Form>
          )}
        </Formik>
      </div>
      
    )
  }
}
